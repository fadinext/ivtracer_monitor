#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /modbus/__init__.py - Arquivo de inicialização do módulo de comunicação Modbus.
"""
Neste módulo estão incluídos os arquivos necessários para a comunicação serial
Modbus do programa, além de uma função auxiliar para listagem das portas seriais.

Para comunicação Modbus, foi incluído o arquivo mb_core.py da biblioteca pylibmodbus,
um wrapper para libmodbus, ambos escritos por stephane. <https://github.com/stephane/pylibmodbus/>
#Copyright (c) Stéphane Raimbault <stephane.raimbault@gmail.com>
"""

#Indica ao interpretador para importar os seguintes arquivos:
from modbus.mb_core      import ModbusRtu,ModbusTcp
from modbus.serial_ports import serial_ports

