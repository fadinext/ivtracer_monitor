#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /gui/mainwindow.py - Inclui a classe da janela MainWindow.

"""
Arquivo com a classe da janela principal do programa. Todos os
elementos visuais são aqui instanciados na forma de atributos da
classe. Alguns métodos auxiliares são utilizados na espeficiação
de partes com muitos elementos, como as funções tabXUI().
""" 

import sys

from PyQt5.QtGui import QColor,QIcon,QPixmap
from PyQt5.QtCore import Qt,QSize,QTimer
from PyQt5.QtWidgets import (QWidget,QGridLayout,
                                QFrame,QPushButton,QMainWindow,
                                QAction,qApp,QTabWidget,QFormLayout,
                                QHBoxLayout, QVBoxLayout,QLabel,QListWidget,QListView)
import pyqtgraph as pg
from gui.setCustomSize import setCustomSize
from events import _mainwindow

class MainWindow(QMainWindow,_mainwindow.Mixin):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowIcon(QIcon('./gui/res/icon_main.png'))
        self.setWindowTitle('IV Tracer Monitor')
        self.setGeometry(250,0,800,800)
        self.loop_toggle = 0
        from classes import Modbus
        self.modbus     = Modbus()
        self.startup_connection()

        #Sem splash screen
        self.showUI()
        #Com splash screen
        #self.about.show()
        #QTimer.singleShot(3000, self.showUI)
        
        
    def showUI(self):
        self.statusBar()
        
        mainMenu = self.menuBar()

        #Menu 'Arquivo'
        fileMenu = mainMenu.addMenu('Arquivo')
        #Ação de Sair do programa
        exitAct = QAction(QIcon('exit.png'), 'Sair', self)        
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Sair da aplicação.')
        exitAct.triggered.connect(qApp.quit)
        fileMenu.addAction(exitAct)

        #Menu 'Ferramentas'
        fileMenu = mainMenu.addMenu('Ferramentas')
        #Ação de Configuração do Dispositivo
        self.commAct = QAction(QIcon('exit.png'), 'Configurar a conexão.', self)        
        self.commAct.setShortcut('Ctrl+L')
        self.commAct.setStatusTip('Configurar a comunicação do dispositivo.')
        fileMenu.addAction(self.commAct)

        #Ação de Calibragem do Dispositivo
        self.calibAct = QAction(QIcon('exit.png'), 'Calibrar', self)        
        self.calibAct.setShortcut('Ctrl+K')
        self.calibAct.setStatusTip('Calibrar o dispositivo.')
        fileMenu.addAction(self.calibAct)

        #Ação de Atualizar Conexões
        self.refreshAct = QAction(QIcon('exit.png'), 'Atualizar Slaves', self)        
        self.refreshAct.setShortcut('Ctrl+R')
        self.refreshAct.setStatusTip('Atualizar Slaves.')
        fileMenu.addAction(self.refreshAct)

        #Janela de TimeLapse
        self.timelapseAct = QAction(QIcon('exit.png'), 'Time Lapse', self)        
        self.timelapseAct.setShortcut('Ctrl+T')
        fileMenu.addAction(self.timelapseAct)
        
        #Menu 'Ajuda'
        fileMenu = mainMenu.addMenu('Ajuda')
        #Ação de Sobre
        self.aboutAct = QAction(QIcon('exit.png'), '&Sobre', self)        
        self.aboutAct.setStatusTip('Sobre o IV Tracer.')
        fileMenu.addAction(self.aboutAct)

        #FRAME
        self.FRAME_A = QFrame(self)

        self.LAYOUT_A = QGridLayout()
        self.FRAME_A.setLayout(self.LAYOUT_A)
        self.setCentralWidget(self.FRAME_A)

        #Tab Curva/Comunicação
        self.infoTab = QTabWidget()
        self.infoTab.tab1 = QWidget()
        self.infoTab.tab2 = QWidget()
        #self.infoTab.addTab(self.infoTab.tab1,"Curva")
        self.infoTab.addTab(self.infoTab.tab2,"Dispositivos")
        #self.tab1UI()
        self.tab2UI()
        self.LAYOUT_A.addWidget(self.infoTab, *(0,0))

        #Tab Log
        vbox = QVBoxLayout()
        hbox = QHBoxLayout()
        self.modbusTab = QTabWidget()
        self.modbusTab.tab1 = QWidget()
        self.modbusTab.addTab(self.modbusTab.tab1,"Eventos")
        vbox.addWidget(self.modbusTab)
        layout = QVBoxLayout()
        self.modbusTab.tab1.log = QListWidget()
        layout.addWidget(self.modbusTab.tab1.log)
        self.modbusTab.tab1.setLayout(layout)

        #Buttons
        #Botão 'Play'
        self.refreshBtn = QPushButton(text = '')
        img = QIcon('./gui/res/play_icon.png')
        self.refreshBtn.setIcon(img)
        setCustomSize(self.refreshBtn, 100, 50)
        self.refreshBtn.clicked.connect(self.escanear)
        
        #Botão 'Replay'
        self.btnLoop = QPushButton(text = '')
        img = QIcon('./gui/res/replay_icon.png')
        self.btnLoop.setIcon(img)
        setCustomSize(self.btnLoop, 100, 50)
        self.btnLoop.clicked.connect(self.loop)
        self.timer = QTimer()
        self.timer.timeout.connect(self.time_out)
       
        hbox.addWidget(self.refreshBtn)
        hbox.addWidget(self.btnLoop)

        vbox.addLayout(hbox)
        self.LAYOUT_A.addLayout(vbox, *(1,0))

        #PyQtGraph configuration
        ## Switch to using white background and black foreground
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')

        #IV plot widget:
        self.IV_plt = pg.PlotWidget(self)
        vbox = QVBoxLayout()
        vbox.addWidget(QLabel('Gráfico IxV'))
        vbox.addWidget(self.IV_plt)
        self.LAYOUT_A.addLayout(vbox,*(0,1))
        self.IV_plt.showGrid(x=True,y=True)
   
        #PV plot widget
        self.PV_plt = pg.PlotWidget(self)
        vbox = QVBoxLayout()
        vbox.addWidget(QLabel('Gráfico PxV'))
        vbox.addWidget(self.PV_plt)
        self.LAYOUT_A.addLayout(vbox,*(1,1))
        self.PV_plt.showGrid(x=True,y=True)

        #Desenha o gráfico IxV e o gráfico PxV
        
        self.IV_plt.plot([0,30], [0,5], pen=pg.mkPen((2,3), width=2.0), clear=True)
        self.PV_plt.plot([0,30], [0,150], pen=pg.mkPen((1,1), width=2.0), clear=True)
        self.IV_plt.plot([0], [0], pen=None,clear=True)
        self.PV_plt.plot([0], [0], pen=None, clear=True)
        
        self.populate_devices()
        self.startup_connection()
        self.show()

    def tab1UI(self):
      #Tab de informações da Curva
      layout = QFormLayout()
      self.infoTab.lblVoc = QLabel('0.00' + ' V')
      layout.addRow("Voc:", self.infoTab.lblVoc)
      
      self.infoTab.lblIsc = QLabel('0.00' + ' A')
      layout.addRow("Isc:", self.infoTab.lblIsc)

      self.infoTab.lblVmp = QLabel('0.00' + ' V')
      layout.addRow("Vmp:", self.infoTab.lblVmp)
      
      self.infoTab.lblImp = QLabel('0.00' + ' A')
      layout.addRow("Imp:", self.infoTab.lblImp)

      self.infoTab.lblPm = QLabel('0.00' + ' W')
      layout.addRow('Pm:', self.infoTab.lblPm)

      self.infoTab.tab1.setLayout(layout)
    
    def tab2UI(self):
      layout = QVBoxLayout()

      self.lstDev = QListWidget()
     
      layout.addWidget(self.lstDev)
      #self.populate_devices()
      self.lstDev.itemDoubleClicked.connect(self.change_device)

      #self.lstDev.setViewMode(QListView.IconMode)
      self.infoTab.tab2.setLayout(layout)

    