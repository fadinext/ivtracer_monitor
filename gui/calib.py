#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /gui/calib.py - Inclui a classe da janela "Calib"(Calibração) do programa.

from PyQt5.QtWidgets import (QWidget,QPushButton,QTabWidget,QFormLayout,
                             QHBoxLayout,QLineEdit,QVBoxLayout,QLabel,
                             QTextEdit)

from events import _calib

class Calib(QWidget,_calib.Mixin):
    def __init__(self):
        super(Calib, self).__init__()
        self.setWindowTitle('Calibração')
        self.showUI()
        from classes import Modbus
        self.modbus     = Modbus()
        self.closeEvent = self.closeEvent

    def showUI(self):
        self.tabCalib = QTabWidget()
        self.textBox = QTextEdit()
        self.tab1 = Tab1()
        self.tab2 = Tab2()
        self.tab3 = Tab3()
        
        self.tabCalib.currentChanged.connect(self.tabChanged)
        self.tabCalib.addTab(self.tab1, 'Calibração de Corrente')
        self.tabCalib.addTab(self.tab2, 'Calibração de Tensão')
        self.tabCalib.addTab(self.tab3, 'Alteração de SlaveID')

        self.tab1.btnCalibCurrent.clicked.connect(self.btnCalibCurrent_onClick)
        self.tab1.btnCurrent1.clicked.connect(self.ReadCurrent1)
        self.tab1.btnCurrent2.clicked.connect(self.ReadCurrent2)
        self.tab2.btnCalibVoltage.clicked.connect(self.btnCalibVoltage_onClick)
        self.tab2.btnVoltage1.clicked.connect(self.ReadVoltage1)
        self.tab2.btnVoltage2.clicked.connect(self.ReadVoltage2)
        self.tab3.btnSetID.clicked.connect(self.btnSetID_onClick)
        self.tab3.btnReboot.clicked.connect(self.btnReboot_onClick)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.tabCalib)
        mainLayout.addWidget(self.textBox)
        self.textBox.setReadOnly(True)

        self.textBox.setText("Ajuste o primeiro valor.")

        self.setLayout(mainLayout)

    def closeEvent(self, event):
        self.clear()


class Tab1(QWidget):
    def __init__(self):
        super(Tab1, self).__init__()
        tabLayout  = QFormLayout()
        buttonsLayout = QHBoxLayout()
        upperRow      = QHBoxLayout()
        self.read = False

        lblRef = QLabel("Referência")
        lblLido = QLabel("Lido")
        upperRow.addWidget(lblRef)
        upperRow.addWidget(lblLido)

        line1      = QHBoxLayout()
        self.edtCurrent1Ref = QLineEdit()
        self.edtCurrent1Read = QLineEdit()
        self.btnCurrent1     = QPushButton("Ler")
        line1.addWidget(self.edtCurrent1Ref)
        line1.addWidget(self.edtCurrent1Read)
        line1.addWidget(self.btnCurrent1)

        line2      = QHBoxLayout()
        self.edtCurrent2Ref = QLineEdit()
        self.edtCurrent2Read = QLineEdit()
        self.btnCurrent2     = QPushButton("Ler")
        line2.addWidget(self.edtCurrent2Ref)
        line2.addWidget(self.edtCurrent2Read)
        line2.addWidget(self.btnCurrent2)

        self.edtCurrent1Read.setReadOnly(True)
        self.edtCurrent2Read.setReadOnly(True)

        tabLayout.addRow("", upperRow)
        tabLayout.addRow("Ajuste 1[A]: ", line1)
        tabLayout.addRow("Ajuste 2[A]: ", line2)
        tabLayout.addRow("", buttonsLayout)

        self.btnCalibCurrent = QPushButton("Salvar")

        buttonsLayout.addWidget(self.btnCalibCurrent)

        self.setLayout(tabLayout)

class Tab2(QWidget,):
    def __init__(self):
        super(Tab2, self).__init__()
        tabLayout  = QFormLayout()
        buttonsLayout = QHBoxLayout()
        upperRow      = QHBoxLayout()
        lblRef = QLabel("Referência")
        lblLido = QLabel("Lido")
        upperRow.addWidget(lblRef)
        upperRow.addWidget(lblLido)
        self.read = False

        line1      = QHBoxLayout()
        self.edtVoltage1Ref = QLineEdit()
        self.edtVoltage1Read = QLineEdit()
        self.btnVoltage1     = QPushButton("Ler")
        line1.addWidget(self.edtVoltage1Ref)
        line1.addWidget(self.edtVoltage1Read)
        line1.addWidget(self.btnVoltage1)

        line2      = QHBoxLayout()
        self.edtVoltage2Ref = QLineEdit()
        self.edtVoltage2Read = QLineEdit()
        self.btnVoltage2     = QPushButton("Ler")
        line2.addWidget(self.edtVoltage2Ref)
        line2.addWidget(self.edtVoltage2Read)
        line2.addWidget(self.btnVoltage2)

        self.edtVoltage1Read.setReadOnly(True)
        self.edtVoltage2Read.setReadOnly(True)  

        tabLayout.addRow("", upperRow)
        tabLayout.addRow("Ajuste 1[V]: ", line1)
        tabLayout.addRow("Ajuste 2[V]: ", line2)
        tabLayout.addRow("",buttonsLayout)
        
        self.btnCalibVoltage = QPushButton("Salvar")
        buttonsLayout.addWidget(self.btnCalibVoltage)

        self.setLayout(tabLayout)

class Tab3(QWidget):
    def __init__(self):
        super(Tab3, self).__init__()
        tabLayout  = QFormLayout()
        
        self.edtID = QLineEdit()
        self.edtNewID = QLineEdit()
        self.btnSetID = QPushButton("Alterar ID")
        self.btnReboot = QPushButton("Reiniciar dispositivo")

        self.edtID.setReadOnly(True)

        tabLayout.addRow("ID Atual: ",self.edtID)
        tabLayout.addRow("Novo ID: ", self.edtNewID)
        tabLayout.addRow(self.btnSetID)
        tabLayout.addRow(self.btnReboot)
       
        self.setLayout(tabLayout)
    


