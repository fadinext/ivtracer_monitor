#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /gui/setCustomSize.py - Contém a função setCustomSize
from PyQt5.QtGui import QColor,QIcon,QPixmap
from PyQt5.QtCore import Qt,QSize,QTimer
from PyQt5.QtWidgets import (QWidget,QApplication,QSizePolicy,QGridLayout,
                                QFrame,QTextEdit,QPushButton,QMainWindow,
                                QAction,qApp,QTabWidget,QFormLayout,QLineEdit,
                                QHBoxLayout, QVBoxLayout,QLabel,QComboBox,QListWidget)
                                
def setCustomSize(x, width, height):
    
    """Função auxiliar para definir o 
    tamanho de Widgets de forma genérica.
    """

    sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(x.sizePolicy().hasHeightForWidth())
    x.setSizePolicy(sizePolicy)
    x.setMinimumSize(QSize(width, height))
    x.setMaximumSize(QSize(width, height))