#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /gui/about.py - Inclui a classe da janela "About" (Sobre) do programa.

from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import (QWidget,QSizePolicy,QGridLayout,QTextEdit,QVBoxLayout,QLabel)
from gui.setCustomSize import setCustomSize

class About(QWidget):
    def __init__(self):
        super(About, self).__init__()
        self.setWindowTitle('Sobre o programa')
        self.showUI()

    def showUI(self):
        layout = QGridLayout()
        vbox = QVBoxLayout()
        vbox.addStretch(1)
        self.lblLic = QLabel('Desenvolvido sob licença GNU GPL v3.')
        vbox.addWidget(self.lblLic)
        layout.addLayout(vbox,  *(1,1))

        vbox = QVBoxLayout()
        self.logo_alsol = QLabel()
        img = QPixmap('./gui/res/logo_alsol.png')
        self.logo_alsol.setPixmap(img)
        vbox.addWidget(self.logo_alsol)
        #setCustomSize(self.logo_alsol,320,320)
        layout.addLayout(vbox,  *(1,0))

        vbox = QVBoxLayout()
        self.logo_alianca = QLabel()
        img = QPixmap('./gui/res/logo_alianca.png')
        self.logo_alianca.setPixmap(img)
        vbox.addWidget(self.logo_alianca)
        #setCustomSize(self.logo_alianca,320,320)
        layout.addLayout(vbox,*(0,0))

        self.txtSobre = QTextEdit('Caracterizador de curvas para painéis solares fotovoltaicos, versão 0.1 .')
        self.txtSobre.setReadOnly(True)
        vbox = QVBoxLayout()
        vbox.addStretch(0.1)
        vbox.addWidget(self.txtSobre)
        setCustomSize(self.txtSobre,180,80)
        layout.addLayout(vbox,  *(0,1))
        self.setLayout(layout)
        self.setGeometry(350,150,320,320)
        setCustomSize(self,720,480)