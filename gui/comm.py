#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /gui/comm.py - Inclui a classe da janela "Comm"(Configuração
# da conexão) do programa.

from PyQt5.QtWidgets import (QWidget,QPushButton,QFormLayout,QLineEdit,
                             QHBoxLayout, QVBoxLayout,QLabel,QComboBox,QStackedWidget,QLineEdit)

from PyQt5.QtCore import pyqtSignal

from modbus import serial_ports
from events import _comm
from classes import ModbusRtuMain,ModbusTcpMain


class Comm(QWidget,_comm.Mixin):
    def __init__(self):
        super(Comm, self).__init__()
        self.setWindowTitle('Configurações de Conexão')
        from classes import Modbus
        self.modbus     = Modbus()
        self.showUI()

    def showUI(self):
        self.Stack = QStackedWidget (self)
        self.stack1 = QWidget()
        self.stack2 = QWidget()
        self.Stack.addWidget (self.stack1)
        self.Stack.addWidget (self.stack2)

        self.cbModbus = QComboBox()
        self.cbModbus.addItems(['RTU','TCP'])

        self.cbTty = QComboBox()
        try:
            self.cbTty.addItems(list(serial_ports()))
        except Exception as e:
            print(e)

        self.cbBaud = QComboBox()
        self.cbBaud.addItems(['4800','9600', '14400', '19200', '38400', '57600', '115200'])
        baud = self.cbTty.currentText()

        self.cbDBits = QComboBox()
        self.cbDBits.addItems(['8','7'])

        self.cbSBits = QComboBox()
        self.cbSBits.addItems(['1','2'])

        self.cbPar = QComboBox()
        self.cbPar.addItems(['N','E','O'])

        self.btnConnectRTU = QPushButton('Conectar')

        layout = QFormLayout()
        layout.addRow('Porta: '    ,self.cbTty   )
        layout.addRow('Baud Rate: ',self.cbBaud  )
        layout.addRow('Data Bits: ',self.cbDBits )
        layout.addRow('Stop Bits: ',self.cbSBits )
        layout.addRow('Paridade: ' ,self.cbPar   )
        layout.addRow(self.btnConnectRTU)
        self.btnConnectRTU.clicked.connect(self.connect_onClick)
        self.stack1.setLayout(layout)

        layout = QFormLayout()

        self.edtIP = QLineEdit("10.0.97.116")
        self.edtPort = QLineEdit("502")
        self.btnConnectTCP = QPushButton('Conectar')

        layout.addRow('IP:', self.edtIP)
        layout.addRow('Port:', self.edtPort)
        layout.addRow(self.btnConnectTCP)

        self.btnConnectTCP.clicked.connect(self.connectTCP_onClick)

        self.stack2.setLayout(layout)

        layout = QFormLayout()
        layout.addRow('Modbus:'    ,self.cbModbus)
        layout.addWidget(self.Stack)
        self.setLayout(layout)
        self.cbModbus.currentIndexChanged.connect(self.changeModbus)
        #self.cbModbus.setCurrentIndex(1)