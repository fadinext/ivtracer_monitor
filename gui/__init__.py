#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /gui/__init__.py - Arquivo de inicialização do módulo das GUIs do programa.

"""
Este diretório contém as classes das janelas do programa, sendo 
uma classe (janela) por arquivo, no padrão:

nomedajanela.py

As funções(eventos) associadas aos botões e outros Widgets
são especificados na forma de métodos nas classes do 
diretório events, no padrão:

_nomedajanela.py

Importado no arquivo da janela correspondente e 
herdado na declaração da classe, o arquivo
de eventos incluíra os métodos correspondentes aos eventos
de sua janela.
        
"""

#Indica ao interpretador para importar as classes das janelas abaixo:
from gui.mainwindow     import MainWindow
from gui.comm           import Comm
from gui.calib          import Calib
from gui.about          import About
from gui.timelapse      import TimeLapse