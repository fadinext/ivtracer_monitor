#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /gui/timelapse.py - Inclui a classe da janela "TimeLapse" (Sobre) do programa.

from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import (QWidget,QSizePolicy,QGridLayout,QTextEdit,
                             QVBoxLayout,QLabel,QHBoxLayout,QTabWidget,
                             QListWidget,QPushButton)

import pyqtgraph as pg

from events import _timelapse
from gui.setCustomSize import setCustomSize

class TimeLapse(QWidget,_timelapse.Mixin):
    def __init__(self):
        super(TimeLapse, self).__init__()
        self.setWindowTitle('TimeLapse')
        self.dirname = ""
        self.showUI()

    def showUI(self):
        layout = QHBoxLayout()

        #Imagem
        vbox = QVBoxLayout()
        self.currentPic = QLabel()
        #img = QPixmap('./gui/res/logo_alianca.png')
        #self.currentPic.setPixmap(img)
        vbox.addWidget(self.currentPic)
        #setCustomSize(self.currentPic,320,320)
        layout.addLayout(vbox)
        vbox.addStretch(0.3)

        #Tab Widget
        self.tabFiles = QTabWidget()
        self.tab1 = Tab1()
        self.tabFiles.addTab(self.tab1, 'Arquivos do TimeLapse')
        vbox.addWidget(self.tabFiles)

        self.tab1.listFiles.currentItemChanged.connect(self.plot_csv)
        self.tab1.btnFiles.clicked.connect(self.search_dir)

        #PV plot widget
        vbox = QVBoxLayout()
        self.PV_plt = pg.PlotWidget(self)
        self.lblPlot = QLabel('Gráfico PxV')
        vbox.addWidget(self.lblPlot)
        vbox.addWidget(self.PV_plt)
        self.PV_plt.showGrid(x=True,y=True)
        layout.addLayout(vbox)

        self.setLayout(layout)
        self.setGeometry(350,150,320,320)
        setCustomSize(self,720,480)

        self.populate_list(self.dirname)

class Tab1(QWidget):
    def __init__(self):
        super(Tab1, self).__init__()

        hbox = QHBoxLayout()

        self.listFiles = QListWidget()
        hbox.addWidget(self.listFiles)

        vbox = QVBoxLayout()
        self.btnFiles = QPushButton("Procurar...")
        vbox.addWidget(self.btnFiles)
        hbox.addLayout(vbox)

        self.setLayout(hbox)