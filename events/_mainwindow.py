#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /events/_mainwindow.py - Inclui a classe Mixin com os métodos dos eventos da janela
# correspondente. Este arquivo deve ser importado e a classe Mixin herdada na
# classe de sua respectiva janela para a inclusão dos eventos.
import os
from time import sleep
from datetime import datetime,date
import random

from numpy import uint16
import numpy as np

import pyqtgraph as pg
from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot

from classes import MbInputRegs,MbCoils
from interpolate import *
from modbus import serial_ports
class Mixin:
    def refresh_slaves(self):
        self.f_update()
        self.populate_devices()
        
    def escanear(self):
        self.modbus.client.mb.set_response_timeout(3)
        try:
           self.modbus.client.mb.write_bit( MbCoils.MB_IVTRACER_VSWEEP_COIL.value, 1 )
        except:
            pass
    
        sleep(1)
        try:
            x = list(self.modbus.client.mb.read_input_registers(MbInputRegs.MB_IVTRACER_V_START.value,
                int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS)))
            y = list(self.modbus.client.mb.read_input_registers( MbInputRegs.MB_IVTRACER_I_START.value , 
                int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS)))
        except Exception as ex:
            self.statusBar().showMessage('Erro de comunicação.')
            self.modbusTab.tab1.log.addItem(('FALHA: ') + 
            datetime.now().strftime("%Y/%m/%d - %H:%M:%S. ") + ('Leitura no dispositivo 0x%x.' % self.currentSlave))
            print("%s:%s" % (str(ex.__class__.__name__),str(ex)))
            #Limpa os gráficos
            self.IV_plt.plot([0], [0], pen=None,clear=True)
            self.PV_plt.plot([0], [0], pen=None, clear=True)
            return -1

        #Ajusta na escala(divisão por 1000)
        for i,item in enumerate(x):
            x[i] = item/1000
        for i,item in enumerate(y):
            y[i] = item/1000

        
        x_new, y_new = interp_and_ma(x,y)


        #Calcula o vetor de potências
        z = []
        for i in range( 0, len(x_new) ):
            z.append(x_new[i] * y_new[i])

        
        #Desenha o gráfico IxV e o gráfico PxV
        self.IV_plt.plot(x_new, y_new, pen=pg.mkPen((2,3), width=3.0), clear=True)
        self.PV_plt.plot(x_new, z, pen=pg.mkPen((1,1), width=3.0), clear=True)

        #Exporta as leituras
        self.export(x,y, self.currentSlave)

        #Encontrar PMP e mostrá-lo nos gráficos
        #zmp = max(z)
        #vmp = x_new[z.index(zmp)]
        #imp = y_new[z.index(zmp)]

        #self.IV_plt.plot([vmp], [imp], pen=None, symbol='x',clear=False)
        #self.PV_plt.plot([vmp], [zmp], pen=None, symbol='x', clear=False)
        
        #Desenhar Isc e Voc no gráfico
        #isc = y_new[0]
        #self.IV_plt.plot([0],[isc],
        #    pen=None, symbol='p'  ,clear=False)
        #voc = x_new[int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS) -1] 
        #self.IV_plt.plot([voc],[y[int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS) -1]],
        #    pen=None, symbol='p'  ,clear=False)

        #Mostra os pontos calculados
        #self.infoTab.lblVoc.setText('%.3f V' % voc)
        #self.infoTab.lblIsc.setText('%.3f A' % isc)
        #self.infoTab.lblVmp.setText('%.3f V' % vmp)
        #self.infoTab.lblImp.setText('%.3f A' % imp)
        #self.infoTab.lblPm.setText( '%.3f W' % zmp)
        

        self.statusBar().showMessage('Dados adquiridos com sucesso.')
        self.modbusTab.tab1.log.addItem(
        'SUCESSO: ' +  datetime.now().strftime("%Y/%m/%d - %H:%M:%S. ") +
        ('Leitura de %i pontos' % int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS)) + 
        (' no dispositivo 0x%x.' % self.modbus.currentSlave))
        return 0
    
    def loop(self):
        self.loop_toggle
        if(self.loop_toggle == 0):
            self.loop_toggle += 1 
            self.statusBar().showMessage('Loop de leitura iniciado(3s).')
            self.timer.start(3000)
        
        elif(self.loop_toggle == 1):
            self.loop_toggle += 1 
            self.statusBar().showMessage('Loop de leitura iniciado(2s).')
            self.timer.start(2000)

        elif(self.loop_toggle == 2):
            self.loop_toggle += 1 
            self.statusBar().showMessage('Loop de leitura iniciado(1s).')
            self.timer.start(1000)   
        
        elif(self.loop_toggle == 3):
            self.statusBar().showMessage('Loop de leitura desligado.')
            self.timer.stop()
            self.loop_toggle = 0

    def time_out(self):
        self.escanear()

    def startup_connection(self):
        try:
            devs = serial_ports()
        except:
            devs = []

        if not devs ==  []:
            self.client = ModbusRtuMain(devs[0].encode(), 115200, b'N', 8, 2 )
        else:
            self.statusBar().showMessage("""Nenhum dispositivo conectado. Conecte o dispositivo e 
            proceda até o menu  'Configurar a conexão'.""")   
            return -1

        try:
            self.client.mb.connect()
            self.statusBar().showMessage('A conexão está pronta.')
        except:
            self.statusBar().showMessage("""Sem conexão. Proceda até 
a aba 'Comunicação' para ajustar as configurações.""")  
 
    def populate_devices(self):
        self.modbus.slaves = []
        self.lstDev.clear()
        try:
            with open("slaves.csv","r") as f:
                self.modbus.slaves = (f.read()).split(',')
        except:
            print("Não foi encontrada uma lista de slaves.\n")

        if self.modbus.slaves == ['']: 
            self.modbus.slaves = []
        if self.modbus.slaves ==   []: 
            return -1

        for i,device in enumerate(self.modbus.slaves):
            item = QListWidgetItem('TRACER%i - Slave ID: 0x%x'%(i, int(device)) )
            img = QIcon('./gui/res/icon_device.png')
            item.setIcon(img)
            self.lstDev.addItem(item)
    
    def f_update(self):
        #self.modbus.client.mb.set_response_timeout(2)
        self.modbus.slaves = list(range(101,111))     
        for j in range(101,111):
            self.modbus.client.setSlave(j)
            try:
                print(f"Tentando comunicação com o slave {hex(j)}.")
                x = list(self.modbus.client.mb.read_registers( MbHoldingRegs.MB_IVTRACER_SLAVE_ADDR.value , 1))[0]
                
                if x == j:
                    print("Slave 0x%x respondeu.\n" % x)
                    sleep(0.1)
                else:
                    raise Exception
            except Exception as e:
                print(e)
                print(f"Removendo {hex(j)}.\n")
                self.modbus.slaves.remove(j)

        with open("slaves.csv","w") as f:
            f.write( (str(self.modbus.slaves).replace('[','').replace(']','') ))

        print("Lista  salva em 'slaves.csv'!")

    def change_device(self,index):
        self.modbus.currentSlave = int(self.modbus.slaves[self.lstDev.currentRow()])
        try:
            self.modbus.client.setSlave(self.modbus.currentSlave)
            self.modbusTab.tab1.log.addItem('SUCESSO: ' + datetime.now().strftime("%Y/%m/%d - %H:%M:%S. ") + ('Slave alterado para 0x%x.' % self.modbus.currentSlave ))
            self.statusBar().showMessage('Dispostivo alterado.')
        except:
            self.modbusTab.tab1.log.addItem('FALHA: ' + datetime.now().strftime("%Y/%m/%d - %H:%M:%S. ") + ('Slave alterado para 0x%x.' % self.modbus.currentSlave ))
            self.statusBar().showMessage('Falha na alteração de dispositivo.')

    def export(self,x,y,slave):
        dirName = str(date.today())
        try:
            os.mkdir(dirName)
        except:
            pass
        fileNamePrefix = (datetime.now()).strftime("%Y-%m-%d_%H-%M-%S")
        with open(f"./{dirName}/{fileNamePrefix}_{hex(int(slave))}.csv","w") as f:
                for i in range(0,len(x)):
                    f.write(f"{x[i]},{y[i]}\n")
        
    """
    def escanear_test(self):
        #Calcula o vetor de potências
        x = range(0,100)
        y = random.sample(range(1, 1000), 100)
        z = []
        for i in range( 0, int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS) ):
            z.append(x[i]*y[i])

        #self.export(x,y,slave)

        #Desenha o gráfico IxV e o gráfico PxV
        self.IV_plt.plot(x, y, pen=pg.mkPen((2,3), width=2.0), clear=True)
        self.PV_plt.plot(x, z, pen=pg.mkPen((1,1), width=2.0), clear=True)

        #Encontrar PMP e mostrá-lo nos gráficos
        zmp = max(z)
        vmp = x[z.index(zmp)]
        imp = y[z.index(zmp)]

        self.IV_plt.plot([vmp], [imp], pen=None, symbol='x',clear=False)
        self.PV_plt.plot([vmp], [zmp], pen=None, symbol='x', clear=False)
        
        #Desenhar Isc e Voc no gráfico
        isc = y[0]
        self.IV_plt.plot([0],[isc],
            pen=None, symbol='p'  ,clear=False)
        voc = x[int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS) -1] 
        self.IV_plt.plot([voc],[y[int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS) -1] ],
            pen=None, symbol='p'  ,clear=False)

        #Mostra os pontos calculados
        #self.infoTab.lblVoc.setText('%.3f V' % voc)
        #self.infoTab.lblIsc.setText('%.3f A' % isc)
        #self.infoTab.lblVmp.setText('%.3f V' % vmp)
        #self.infoTab.lblImp.setText('%.3f A' % imp)
        #self.infoTab.lblPm.setText( '%.3f W' % zmp)

        t_up = pg.TextItem('Voc', (255, 255, 255), anchor=(0, 0))
        t_up.setPos(0.5, 0.5)

        self.statusBar().showMessage('Dados adquiridos com sucesso.')
        self.modbusTab.tab1.log.addItem(
        'SUCESSO: ' +  datetime.now().strftime("%Y/%m/%d - %H:%M:%S. ") +
        ('Leitura de %i pontos' % int(MbInputRegs.MB_IVTRACER_MAX_IV_POINTS)) + 
        (' no dispositivo 0x%x.' % self.currentSlave))"""
        