#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /events/__init__.py - Arquivo de inicialização do módulo dos métodos
# dos eventos associados aos Widgets do programa.

"""
Este diretório contém Mixins para serem herdados nos arquivos de classe
das janelas do programa, sendo  uma classe (janela) por arquivo, no padrão:

_nomedajanela.py

Importado no arquivo da janela correspondente e 
herdado na declaração da classe, o arquivo
de eventos incluíra os métodos correspondentes aos eventos
de sua janela.

Assim, o arquivo nomedajanela.py será do tipo:

from events import _nomedajanela

class Nomedajanela(QMainWindow, _nomedajanela.Mixin):
    def __init__(self, ...):
        ...

"""
