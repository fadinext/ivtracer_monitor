#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /events/_comm.py - Inclui a classe Mixin com os métodos dos eventos da janela
# correspondente. Este arquivo deve ser importado e a classe Mixin herdada na
# classe de sua respectiva janela para a inclusão dos eventos.

from classes import ModbusRtuMain,ModbusTcpMain,NoDeviceException
class Mixin:
    def connect_onClick(self):
        self.device = self.cbTty.currentText().encode()
        self.baud   = int(self.cbBaud.currentText())
        self.dbit   = int(self.cbDBits.currentText())
        self.sbit   = int(self.cbSBits.currentText())
        self.parity = self.cbPar.currentText().encode()
        
        try:
            if(self.device == b''):
                raise NoDeviceException

        except NoDeviceException:
            print("Can't connect to an empty string/IP!")
            return -1
     
        self.modbus.client.tearDown()
        self.modbus.client = ModbusRtuMain(self.device, self.baud,
                        self.parity, self.dbit, self.sbit)

        try:
            self.modbus.client.mb.connect()
            #self.statusBar().showMessage('A conexão está pronta.')
            #self.modbusTab.tab1.log.addItem('SUCESSO: ' +  datetime.now().strftime("%Y/%m/%d - %H:%M:%S . ") + 'Conexão reconfigurada.')
        except:
            #self.statusBar().showMessage(Sem conexão. Checou permissões, inicialização do programa na placa e conexões físicas?)
            #self.modbusTab.tab1.log.addItem('FALHA: ' +  datetime.now().strftime("%Y/%m/%d - %H:%M:%S . ") + 'Conexão reconfigurada.')
            pass
        #self.f_update()
        #self.populate_devices()

    def changeModbus(self):
        if self.cbModbus.currentText() == 'RTU':
            self.Stack.setCurrentIndex(0)
        elif self.cbModbus.currentText() == 'TCP':
            self.Stack.setCurrentIndex(1)

    def connectTCP_onClick(self):
        self.ip = self.edtIP.text()
        self.port = int(self.edtPort.text())

        self.modbus.client.tearDown()
        self.modbus.client =  ModbusTcpMain(self.ip,self.port)

        try:
            self.modbus.client.mb.connect()
        except:
            pass