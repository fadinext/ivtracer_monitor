import os

from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QFileDialog
import csv
import pyqtgraph as pg

class Mixin():
    def populate_list(self,dirname):
        self.tab1.listFiles.clear()
        self.files = []
        try:
            for filename in os.listdir(dirname):
                if filename.endswith('.csv'):
                    self.files.append(filename)
        except:
            pass

        self.PV_plt.plot([0], [0], pen=None, clear=True)
        self.tab1.listFiles.addItems(self.files)

    def search_dir(self):
        try:
            file = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        except:
            pass
        else:
            self.dirname = file
            self.populate_list(self.dirname)
    
    def plot_csv(self):
        try:
            filename = str(self.tab1.listFiles.currentItem().text())
            if filename == None:
                raise Exception
        except:
            return -1
            
        x , y , z= [] , [] , []
        self.PV_plt.setRange(xRange=[0,33])
        try:
            with open(self.dirname +"/"+ filename, "r") as csvfile:
                try:
                    rows =  csv.reader(csvfile, delimiter=';')
                    for row in rows:
                        x.append(float(row[0]))
                        y.append(float(row[1]))
                except:
                    try:
                        rows =  csv.reader(csvfile, delimiter=',')
                        for row in rows:
                            x.append(float(row[0]))
                            y.append(float(row[1]))
                    except Exception as e:
                        print(e)
        except Exception as e:
            print(e)
            
        for i in range(len(x)):
            z.append(x[i] * y[i])

        self.PV_plt.plot(x, z, pen=pg.mkPen((1,1), width=3.0), clear=True)
        self.PV_plt.show()
        self.lblPlot.setText('Gráfico PxV: '+ filename[:-9:].replace('_','  '))

        img = QPixmap(self.dirname + "/" + filename[:-4:] + '.jpg')
        self.currentPic.setPixmap(img)

    def closeEvent(self,event):
        self.PV_plt.plot([0], [0], pen=None, clear=True)
        self.tab1.listFiles.clear()
