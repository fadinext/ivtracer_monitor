#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# /events/_calib.py - Inclui a classe Mixin com os métodos dos eventos da janela
# correspondente. Este arquivo deve ser importado e a classe Mixin herdada na
# classe de sua respectiva janela para a inclusão dos eventos.

from numpy import uint16

from classes import MbHoldingRegs,MbInputRegs,MbCoils
class Mixin:
    def btnCalibCurrent_onClick(self):
        if(self.tab1.edtCurrent1Ref.text() == '' or self.tab1.edtCurrent2Ref.text()== ''):
            return -1

        y1 = float(self.tab1.edtCurrent1Ref.text())
        y2 = float(self.tab1.edtCurrent2Ref.text())
        x1 = float(self.tab1.edtCurrent1Read.text())
        x2 = float(self.tab1.edtCurrent2Read.text())

        a,b = self.calculaCoefs(x1,x2,y1,y2)
        a = uint16(a*1000)
        b = uint16(b*1000)

        self.textBox.setText("""Calibração concluída.
        a=%i b=%i.""" % (a,b))
        self.setCurrentCalibration([a,b])

    def btnCalibVoltage_onClick(self):
        if(self.tab2.edtVoltage1Ref.text() == '' or self.tab2.edtVoltage2Ref.text()== ''):
            return -1

        y1 = float(self.tab2.edtVoltage1Ref.text())
        y2 = float(self.tab2.edtVoltage2Ref.text())
        x1 = float(self.tab2.edtVoltage1Read.text())
        x2 = float(self.tab2.edtVoltage2Read.text())

        a,b = self.calculaCoefs(x1,x2,y1,y2)
        a = uint16(a*1000)
        b = uint16(b*1000)

        self.textBox.setText("""Calibração concluída.
        a=%i b=%i.""" % (a,b))
        
        self.setVoltageCalibration([a,b])

    def btnSetID_onClick(self):
        ID = int(self.edtNewID.text())
        self.setSlaveID(ID)
    
    def btnReboot_onClick(self):
        self.close()
        self.Reboot()
        
    def calculaCoefs(self,x1,x2,y1,y2):
        """Calcula os coeficientes de uma reta na forma y=ax + b,
        dados dois pontos quaisquer (x1,y1) e (x2,y2).
        """

        a = (y2 - y1)/(x2 - x1)
        b =  y2 - a*x2

        return([a,b])

    def tabChanged(self):
        index = self.tabCalib.currentIndex()
        if   index  == 0:
            self.textBox.show()
            self.setVRef(0)
        elif index  == 1:
            self.textBox.show()
            self.setVRef(50000)
        elif index  == 2:
            self.textBox.hide()
            try:
                x = list(self.modbus.client.mb.read_registers( MbHoldingRegs.MB_IVTRACER_SLAVE_ADDR.value , 1))
                self.tab3.edtID.setText(str(hex(x[0])))
            except:
                self.tab3.edtID.setText("Nenhum slave conectado.")
    
    def clear(self):
        self.tabCalib.setCurrentIndex(0)
        self.setVRef(50000)
    
    def ReadVoltage1(self):
        self.setVoltageCalibration([1000,0])
        v,i  = self.ReadOpPoint()

        self.edtVoltage1Read.setText(str(v))
        self.textBox.setText("Ajuste o segundo valor e pressione 'Iniciar' novamente.")    

    def ReadVoltage2(self):
        self.setVoltageCalibration([1000,0])
        v,i  = self.ReadOpPoint()
        self.edtVoltage2Read.setText(str(v))
        self.textBox.setText("Valores carregados. Pressione 'Salvar' para realizar a calibração.") 
          

    def ReadCurrent1(self):
        self.setCurrentCalibration([1000,0])
        v,i  = self.ReadOpPoint()
        self.edtCurrent1Read.setText(str(i))
        self.textBox.setText("Ajuste o segundo valor e pressione 'Iniciar' novamente.")
        
 
    def ReadCurrent2(self):
        self.setCurrentCalibration([1000,0])
        v,i  = self.ReadOpPoint()
        self.edtCurrent2Read.setText(str(i))
        self.textBox.setText("Valores carregados. Pressione 'Salvar' para realizar a calibração.")


    def setVRef(self,value):
            try:
                self.modbus.client.mb.write_register(MbHoldingRegs.MB_IVTRACER_VREF,value)
            except:
                pass

    def setVoltageCalibration(self, coefs):
        #self.modbus.client.mb.set_response_timeout(2)
        self.modbus.client.mb.write_registers(MbHoldingRegs.MB_IVTRACER_VGAIN,[coefs[0]])
        self.modbus.client.mb.write_registers(MbHoldingRegs.MB_IVTRACER_VOFFSET,[coefs[1]])

    def setCurrentCalibration(self, coefs):
        #self.modbus.client.mb.set_response_timeout(2)
        self.modbus.client.mb.write_registers(MbHoldingRegs.MB_IVTRACER_IGAIN,   [coefs[0]] )
        self.modbus.client.mb.write_registers(MbHoldingRegs.MB_IVTRACER_IOFFSET,([coefs[1]]))

    def Reboot(self):
        try:
            self.modbus.client.mb.write_bit(int(MbCoils.MB_IVTRACER_RST_COIL), 1 )
        except:
            pass

    def setSlaveID(self, value):
        #self.modbus.client.mb.set_response_timeout(2)
        self.modbus.client.mb.write_register( MbHoldingRegs.MB_IVTRACER_SLAVE_ADDR.value , value)

    def ReadOpPoint(self):
        self.modbus.client.mb.write_bit( int(MbCoils.MB_IVTRACER_RD_OP_POINT), 1)
        i = list(self.modbus.client.mb.read_input_registers(MbInputRegs.MB_IVTRACER_I_OPPOINT,1))[0] / 1000
        v = list(self.modbus.client.mb.read_input_registers(MbInputRegs.MB_IVTRACER_V_OPPOINT,1))[0] / 1000
        return (v,i)
        
    
        
