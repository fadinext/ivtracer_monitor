
from scipy import interpolate
import numpy as np

import csv
#import matplotlib.pyplot as plt

def remove_duplicates(x,y):
    # Combine lists into list of tuples
    points = zip(x, y)
    # Sort list of tuples by x-value
    points = sorted(points, key=lambda point: point[0])
    # Split list of tuples into two list of x values any y values
    x,y = zip(*points)   

    x = list(x)
    y = list(y)

    x_new = []
    y_new = []

    for i,item in enumerate(x):
        if not item in x_new:
            x_new.append(x[i])
            y_new.append(y[i])

    return x_new,y_new

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


def test():
    x = []
    y = []

    try:
        with open("./2019-04-26/2019-04-26_13_58_34_0x66.csv", "r") as csvfile:
            rows =  csv.reader(csvfile, delimiter=';')
            for row in rows:
                x.append(float(row[0]))
                y.append(float(row[1]))
    except Exception as e:
        print(e)
        
    
    x_new, y_new = interp_and_ma(x,y)

    plt.plot(x_new, y_new, '-')

    plt.show()


def interp_and_ma(x,y):
    x_new, y_new = remove_duplicates(x,y)
    x2 = np.linspace( min(x_new) , max(x_new),
        int((max(x_new) - min(x_new) )/0.1))
    f = interpolate.interp1d(x_new, y_new, assume_sorted = False)(x2)

    y = moving_average(f,5)

    return x2[4:], y


if __name__ == "__main__":
    test()



