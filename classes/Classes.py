import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication
import configparser

from interpolate import *
class MainApp(object):
    def __init__(self):
        self.QApp = QApplication(sys.argv)
        from gui import MainWindow,About,Calib,Comm,TimeLapse
        self.mainwindow = MainWindow()
        self.calib      = Calib()
        self.comm       = Comm()
        self.about      = About()
        self.modbus     = Modbus()
        self.timelapse  = TimeLapse()
        
        self.configs = configparser.ConfigParser()
    
        
    def run(self):
        self.mainwindow.aboutAct.triggered.connect(self.about.show)
        self.mainwindow.commAct.triggered.connect(self.comm.show)
        self.mainwindow.calibAct.triggered.connect(self.calib.show)
        self.mainwindow.timelapseAct.triggered.connect(self.timelapse.show)
        self.mainwindow.refreshAct.triggered.connect(self.mainwindow.refresh_slaves)

        self.configs.sections()
   
        if(self.configs.read('ivtracer.cfg')):
            pass
        else:
            with open('ivtracer.cfg','w') as f:
                f.write("""#Arquivo de configuração do IV Tracer
    #
    #Preenchimento padrão:
    [COMM-RTU]
    Device   = /dev/ttyUSB0
    BaudRate = 9600
    DataBits = 8
    Parity   = N
    StopBits = 1
    [COMM-TCP]
    IP   = 127.0.0.1
    PORT = 502
    [PLOTTING]
    Interpolation = False
    #
                """)
       
        sys.exit(self.QApp.exec_())
        self.modbus.client.tearDown()
        
class Modbus(object):
    class __Modbus:
        def __init__(self):
            self.client       = ModbusTcpMain("127.0.0.1",502)
            self.slaves       = list(range(1,247))
            self.currentSlave = 0
        
        def __str__(self):
            return repr(self)
    instance = None

    def __new__(cls):
        if not Modbus.instance:
            Modbus.instance = Modbus.__Modbus()
        return Modbus.instance

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def __setattr__(self, name):
        return setattr(self.instance, name)

class NoDeviceException(Exception):
    """Raised when you try to connect to an empty string serial port."""
    pass

#*************************************#
#********* Classes Modbus ************#
#*************************************#

from enum import IntEnum
from modbus.mb_core import ModbusRtu,ModbusTcp
class ModbusRtuMain():
    def __init__(self, device , baud, parity, data_bit, stop_bit):
        self.mb = ModbusRtu(device, baud, parity, data_bit, stop_bit)

    def setSlave(self,slave):
        self.mb.set_slave(slave)

    def tearDown(self):
        self.mb.close()

class ModbusTcpMain():
    def __init__(self, ip, port ):
        self.mb = ModbusTcp(ip,port)

    def setSlave(self,slave):
        self.mb.set_slave(slave)

    def tearDown(self):
        self.mb.close()

class MbInputRegs(IntEnum):
    MB_IVTRACER_MAX_IV_POINTS      =    100

    MB_IVTRACER_I_OPPOINT          =    0
    MB_IVTRACER_V_OPPOINT          =    1
    MB_IVTRACER_I_START            =    2
    MB_IVTRACER_V_START            =    MB_IVTRACER_I_START + 200 + 1

class MbCoils(IntEnum):
    MB_IVTRACER_RD_OP_POINT        =    2
    MB_IVTRACER_VSWEEP_COIL        =    3
    MB_IVTRACER_FACTORY_RST_COIL   =    4    
    MB_IVTRACER_RST_COIL           =    5     

class MbHoldingRegs(IntEnum):
    MB_IVTRACER_VREF               =     0
    MB_IVTRACER_IGAIN              =     1   
    MB_IVTRACER_IOFFSET            =     2   
    MB_IVTRACER_VGAIN              =     3   
    MB_IVTRACER_VOFFSET            =     4   
    MB_IVTRACER_SLAVE_ADDR         =     5   