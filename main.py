#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# main.py - Ponto de entrada do programa. Compilar SEMPRE este arquivo.

"""
IV Tracer Monitor v0.1
Traçador de curvas para geradores fotovoltaicos

Desenvolvido por:
Marcelo Fadini      marcelofadini@hotmail.com

Desenvolvido sob licença GNU GPL v3
"""

from classes import MainApp

if __name__ == "__main__":
    app = MainApp()
    app.run()